### General

* This site is a closed group and only for developers, Admins and a few chosen players.

### Goals

* Efficient task management is for the mentioned people of the group. [**Here's the Task List**](https://gitlab.com/spacetrek/stne-tasks/issues)
* Concentrate work on tasks, without having to rummage through tickets or forum posts 
* The source text changes (commits) on the basis of the issue (by stne tasks#<issue id>).
* This site is purely a management site and does not contain the source code from STNE

### Guide

* Record only meaningful tasks
* Bugs must have been previously validated
* Fixed bugs, false reports, out-of-date bugs or rejected ideas should be closed regularly.
* If possible, only include ideas that have been previously validated / discussed (wherever). Do not blindly capture ideas. The goal is that only ideas that are captured which will be implemented at a later date.
* Since not only Admins have access to this page, critical data is not lost. This may only be done  in the internal source-project.
* Keep a brief task description and focus on the essentials. No wandering.
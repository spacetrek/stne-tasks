# SSL Certificate
## Let's Encrypt
For example with the CA _Lets Encrypt_ .  
#### Setup on Debian:
##### Prerequisites
* Google-Account:
    * manage LoadBalancer
    * manage DNS records
    * add new SSL-certificates
* some UNIX Server-Console to execute certboot with root-priviliges
##### Console
Installation:  
normal with packet-manager (maybe old version) (maybe without stretch-backports)
```
sudo apt-get install python-certbot-apache -t stretch-backports
```
or manual script:
```
wget https://dl.eff.org/certbot-auto
chmod a+x certbot-auto
```

During the registration progress, the certbot tool gives future instructions to follow.
```
sudo ./certbot-auto certonly --manual --preferred-challenges dns-01 -d '*.stne.net,*.de.stne.net,*.en.stne.net,*.es.stne.net,*.fr.stne.net'
sudo cat /etc/letsencrypt/live/stne.net-0001/fullchain.pem
sudo cat /etc/letsencrypt/live/stne.net-0001/privkey.pem
```

To keep things simple, you can just use a ready-to-use container:
```
docker run -it --name certbot --rm -v "/etc/letsencrypt:/etc/letsencrypt"  -v "/var/lib/letsencrypt:/var/lib/letsencrypt" certbot/dns-google certonly --manual --preferred-challenges dns-01 -d 'stne.net,*.stne.net,*.de.stne.net,*.en.stne.net,*.es.stne.net,*.fr.stne.net' --server https://acme-v02.api.letsencrypt.org/directory --agree-tos --email sebastian.loncar@gmail.com
```

On the same machine one can expand the certificate with other domains,
run the same command with some more domains in -d, 
certbot will ask if you want to expand the certificate.
During this process there is one validation only for the new added domains.
So you can add to one wildcard Domain the top-domain, ignore the instruction for 
the already validated domain and create only the dns entry for the new ones.

It's important that you are waiting for DNS propagation!

### External Links
* https://certbot.eff.org/docs/using.html#manual
* https://community.letsencrypt.org/t/getting-wildcard-certificates-with-certbot/56285
* https://certbot-dns-google.readthedocs.io/en/latest/#credentials

### ToDo's
* [ ] Setup cronjob with certbot-dns-google plugin for autorenew from the certs
* [ ] Possible: use docker-container for the certbot-dns-plugin

# Google LoadBalancer
* go to GoogleCloudPlatform->project stne->network
* add required DNS entrys (instructions from certbot) under Cloud DNS
* go to ->LoadBalancer->Edit->FrontEnd
    * add new rule for https
    * add new certificate with data from the `cat`'s-command
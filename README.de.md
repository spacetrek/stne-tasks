English version: https://gitlab.com/spacetrek/stne-tasks/blob/master/README.en.md

### Allgemeines

* Diese Seite ist eine geschlossene Gruppe nur für Entwickler, Admins und auserwählte Spieler

### Ziele

* Effiziente Aufgabenverwaltung für besagte Personengruppe. [**Hier geht es zu den Aufgaben**](https://gitlab.com/spacetrek/stne-tasks/issues)
* Konzentriertes Arbeiten an den Aufgaben, ohne Tickets oder Forenbeiträge durchwühlen zu müssen
* Bei den Quelltext-Änderungen (Commits) wird auf das jeweilige Issue (per stne-tasks#<issue-id>) verwiesen.
* Diese Seite ist eine reine Verwaltungsseite und enthält keine  Quelltexte von STNE

### Leitfaden

* NUR sinnvolle Aufgaben aufnehmen
* Bugs müssen zuvor validiert worden sein
* Behobene Bugs, Falsch-Meldungen, nicht mehr aktuelle Bugs oder verworfene Ideen sollen regelmäßig geschlossen werden. Es gilt Karteileichen zu vermeiden.
* Nach Möglichkeit nur Ideen aufnehmen, welche zuvor validiert/besprochen wurden (wo auch immer). Nicht blind Ideen erfassen. Ziel ist es, dass nur Ideen erfasst werden welche zu einem späteren Zeitpunkt auch umgesetzt werden.
* Da nicht nur Admins Zugriff auf diese Seite haben, haben kritische Daten hier nichts verloren. Dies darf  nur im internen source-Projekt erfolgen.
* Aufgabenbeschreibung kurz halten und sich auf das Wesentliche konzentrieren. Kein Abschweifen.
 
### Sprache der Kommunikation

* Es sind nur Englisch und Deutsch zulässig.
* Maschinelle Übersetzung ohne manuelle Nachkontrolle ist nicht erwünscht.
* Bei deutschen Texten darf der Autor oder ein anderes Mitglied nach freiem Ermessen den Text unterhalb des deutschen Textes in Englisch übersetzen (issue bearbeiten), als Dual-Sprache.
* Dual-Sprache in der Aufgabenbeschreibung ist keinesfalls gefordert! Nur dort anwenden, wo es Sinn macht.
* Es dürfte unnötig sein direkt in Englisch gefasste Aufgaben zusätzlich ins Deutsche zu übersetzen. Es wird davon ausgegangen, dass die meisten deutschen Mitglieder einen Grundwortschatz haben um Englisch lesen zu können.
* Wer es sich zutraut direkt in Englisch zu schreiben, sollte dies tun.

### Task Management

* Weight: Geschätze Zeiteinheiten zum erledigen der Aufgabe. Eine Zeiteinheit = 15 Minuten. 
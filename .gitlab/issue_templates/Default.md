# What problem do you want to solve?

Please describe your problem.

# Why is it a problem?

This is especially important if it is a subjective change.

# What would you imagine as a solution?

This doesn't need to be too detailed, but it can help to give the devs
some pointers.

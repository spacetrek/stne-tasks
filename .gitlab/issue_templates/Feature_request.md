# What feature do you want to suggest?

Please write about your feature suggestion. The more detailed,
the better.

# How would it improve the playing experience?

This is important as we do not want features that are unbalanced, for
example.

# (Optional) how would you implement the feature?

If the feature would change the game balance, you can add some
additional info on how to ensure the game stays fun.
